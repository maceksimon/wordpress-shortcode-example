# Shortcode Jukebox

Example Wordpress plugin for registering a shortcode.

## Use

Browse the example code or copy the files into a **jukebox** folder inside your Wordpress /plugins directory.

This should illustrate a blog post which will be uploaded shortly.
