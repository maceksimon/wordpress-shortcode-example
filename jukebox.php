<?php
/**
 * Plugin Name: Jukebox Plugin
 * Description: Displays a song names and a shuffle button
 */

function jukebox_shortcode() {

  wp_enqueue_style( 'jukebox-css', plugin_dir_url(__FILE__) . '/jukebox.css');
  wp_enqueue_script( 'jukebox-js', plugin_dir_url(__FILE__) . '/jukebox.js');

  $output  = '';
  $output .= '<div class="jukebox">';
  $output .= '<span class="jukebox-display">Nothing playing yet...</span>';
  $output .= '<button class="jukebox-button">Shuffle</button>';
  $output .= '</div>';

  return $output;
}

add_shortcode( 'jukebox', 'jukebox_shortcode' );