// get some data
const songs = [
  "Kaloli - Tewali Sukali",
  "Kate NV - Ca commence par moi",
  "King Krule - Lizard State",
  "Moses Sumney - Virile",
];

// get your buttons and display fields
const displays = document.getElementsByClassName("jukebox-display");
// you need this in order to use .forEach()
const buttons = Array.from(document.getElementsByClassName("jukebox-button"));

// add event listeners for each button
buttons.forEach((button, index) => {
  button.addEventListener("click", () => {
    // generate random element from an array, and set it as the display value
    displays[index].innerHTML = songs[Math.floor(Math.random() * songs.length)];
  });
});
